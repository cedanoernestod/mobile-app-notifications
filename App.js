import React, {Fragment} from 'react';
import { SafeAreaView, StatusBar, Text} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import { mapping, light as lightTheme } from '@eva-design/eva';
import { ApplicationProvider, Layout } from 'react-native-ui-kitten';
import LoginPage from './src/pages/pages/login/Login';
import Home from './src/pages/pages/home/Home';
import io from 'socket.io-client';
import { getUserId } from './src/api/auth';
import PushNotification from 'react-native-push-notification';
import { notificationSubject } from './src/api/notifications';
const Wrapper = (Component) => {
  return () => (
    <ApplicationProvider
    mapping={mapping}
    theme={lightTheme}>
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex:1 }} >
        <Component/>
      </SafeAreaView>
    </Fragment>
  </ApplicationProvider>
  );
};
// Connection to socket.io
const socket = io('http://192.168.178.21:5000');
socket.on('notification', async ({notification, userId}) => {
  const { titulo, descripcion } = notification;
  const idUserLogged = parseInt(await getUserId());
  // Verify that the notification belongs to the logged user
  if (userId === idUserLogged)  {
    PushNotification.localNotification({
      title: titulo,
      message: descripcion,
    });
    notificationSubject.next();
  }
});
// Navigation structure
const MainNavigator = createStackNavigator({
  Login: Wrapper(LoginPage),
  Home:  {
    screen: Wrapper(Home),
    navigationOptions:  {
      title: 'Notificaciones',
      headerLeft: null,
      gesturesEnabled: false,
}
  }
}, { initialRouteName:  'Login'});

export default createAppContainer(MainNavigator);
