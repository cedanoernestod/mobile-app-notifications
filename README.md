**App**
Aplicacion desarrollada con `react-native`

**Directorios**
*  `android` Codigo nativo de Android
*  `ios` Codigo nativo de IOs
*  `node_modules` Dependencias del proyecto
*  `src` codigo del proyecto

**Reemplazar URLs de la API**
para establecer la comunicacion con la aplicacion y el backend se tiene que especificar la direccion ip del servidor donde este ejecutandose el backend en el siguiente archivo
`src/api/axios.js`

**Instrucciones**
*  Instalar `nodejs` https://nodejs.org/en/
*  Instalar `react-native` siguiendo esta documentacion https://facebook.github.io/react-native/docs/getting-started

**Ejecutar la aplicacion**
*  Navegar al directorio principal de repositorio y ejecutar `npm install` y `react-native run-android`
*  