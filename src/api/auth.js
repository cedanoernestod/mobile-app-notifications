import axiosInstance from "./axios";
import AsyncStorage from '@react-native-community/async-storage';

export const auth = async (username, password) => {
    try {
        const response = await axiosInstance.get('users/auth/', { params : {username, password }});
        console.log(response);
        if (response.data) {
            const { id } = response.data;
            const token = response.headers['x-auth-token'];
            await AsyncStorage.setItem('x-auth-token', token);
            await AsyncStorage.setItem('user-id', String(id));
            axiosInstance.defaults.headers.common['x-auth-token'] = token;
        }
        return !!response.data;
    } catch(e) {
        console.log(e);
    }
}
export const logOut = async () => {
    try {
        await AsyncStorage.removeItem('x-auth-token');
        await AsyncStorage.removeItem('user-id');
        axiosInstance.defaults.headers.common['x-auth-token'] = '';
    } catch(e) {
        console.log(e);
    }
}

export const isLogged = async () => {
    try {
        const token = await AsyncStorage.getItem('x-auth-token');
        if (token) {
            axiosInstance.defaults.headers.common['x-auth-token'] = token;
        }
    return token
    } catch(e) {
        alert('Error al obtener el token');
    }
}
export const getUserId = async () => {
    try {
        const id = await AsyncStorage.getItem('user-id');
    return id;
    } catch(e) {
        alert('Error al obtener el id del usuario');
    }
}