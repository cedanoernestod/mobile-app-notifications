import axiosInstance from "./axios";
import { getUserId } from './auth';
import { Subject } from 'rxjs';

export const getNotifications = async () => {
    try {
        const userId = await getUserId();
        const response = await axiosInstance.get(`notifications/user/${userId}`);
        return response.data;
    } catch(e) {
        console.log(e);
    }
}
export const notificationSubject = new Subject();