import React, { Component } from 'react';
import {
    List,
    ListItem,
  } from 'react-native-ui-kitten';
  //import NotificationsIOS, { NotificationsAndroid } from 'react-native-notifications';
  import { Platform } from 'react-native';
import { getNotifications, notificationSubject } from '../../../api/notifications';
var PushNotification = require('react-native-push-notification');
import io from 'socket.io-client';
import { getUserId } from '../../../api/auth';

export default class Home extends Component {
  state = {
    data: []
  };
  constructor(props) {
    super(props);
    if (Platform.OS === 'ios') {
    } else {
    }
    PushNotification.requestPermissions();
  }
  async fetch() {
    try {
      const data = await getNotifications();
      this.setState({ data});
      } catch(e) {
        alert('Error al cargar las notificaciones')
      }

  }
  async componentDidMount() {
    this.fetch();
    try {
      notificationSubject.subscribe(() => {
        this.fetch();
      });
    } catch(e) {
      console.log(e);
    }
  }
    renderItem = ({item}) => {
        return (
          <ListItem
            title={item.titulo}
            description={item.descripcion}
          />
        );
      };
    render() {
        return(
            <List
            data={this.state.data}
            renderItem={this.renderItem}
          />
        );
    }
}