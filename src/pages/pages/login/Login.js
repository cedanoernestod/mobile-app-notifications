import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { Input, Button } from 'react-native-ui-kitten';
import { auth, isLogged } from '../../../api/auth';
import { withNavigation } from 'react-navigation';
class LoginPage extends Component {
    constructor(props) {
        super(props);
    }
    state = { 
        isValid: null,
        isSolved: false,
        isLogged: true,
        badCredentials: null,
        user: 'ernesto',
        password: 'test'
    };
    async componentDidMount() {
     if (await isLogged()) {
        this.props.navigation.navigate('Home');
        this.setState({isSolved: true, isLogged: true});
     }
    }
    async login () {
        try {
            const { user, password } = this.state;
            let isValid = !! (password && user);
            if (isValid) {
               const logged = await auth(user, password);
               if (logged) {
                   this.props.navigation.navigate('Home');
               }
               else {
                   isValid = false;
                   alert('Usuario o contrasena incorrecta');
               }
            }
            this.setState({isValid});
        } catch(e) {
            console.log(e);
        }
    }

    setValue(field, value) {
        this.setState({
          [field]: value  
        });
    }
    render () {
        const { isValid, user, password, isSolved } = this.state;
        return (
            <View style={styles.page}>
                <View style={styles.fields}>
                    <Input 
                        status={ isValid === false ? 'danger' : 'primary'}
                        value={user}
                        onChangeText={(value) => this.setValue('user', value)}
                        label="usuario"></Input>
                    <Input
                        secureTextEntry={true}
                        status={ isValid === false ? 'danger' : 'primary'}
                        value={password}
                        onChangeText={(value) => this.setValue('password', value)}
                        label="contrasena"></Input>
                </View>
                    <Button appearance={'primary'} onPress={() => this.login.apply(this)}> Acceder</Button>
            </View>
        );
    } 
}
const styles = StyleSheet.create({
    page: {
        flex: 1,
        padding: 10,
        alignItems: "center",
        justifyContent: "center",
    },
    fields: {
        paddingBottom: 20,
        width: '100%'
    }
})
export default withNavigation(LoginPage);
